# Introduction

- [Introduction](#introduction)
- [Règles du jeu](#règles-du-jeu)
- [Lancement du jeu](#lancement-du-jeu)
- [Structure de l'application](#structure-de-lapplication)


Afin de découvrir la **Réalité Virtuel**, nous avons utilisé une Kinect de Xbox pour récupérer les données accessible par celle-ci pour enfin créer un jeu en C#, ci vous ne savez pas à quoi ca ressemble voici un exemple ci-dessous.
<br>
<img src="./images/kinect.png" width="600"/>

Nous avons a réalisé un certains nombre de TP afin d'acquérir les fondamentaux de cet outil au cours d'une période de quatres semaines. Le but était donc de développer notre propre jeu à la fin de la période.

Ces TP ont été réalisé par RANDON Noan et JOLYS Enzo élèves de troisième année de BUT Informatique.

**Organisation**
**TP1** : Présent sur la branche TP1
**TP2 Partie 1** : Présent sur la branche TP1
**TP2 Partie 2** : Présent sur la branche TP1
**TP2 Partie 3** : Présent sur la branche TP3 & master
**TP3** : Présent sur la branche TP3 & master

# Lancement du jeu

Il faut savoir que si vous souhaitez lancer ce projet, il vous faudra être sur Windows et avoir une kinect, si c'est le cas installez le SDK suivant:
- [**Kinect pour Windows SDK 2.0**](https://www.microsoft.com/en-us/download/details.aspx?id=44561)

Pour lancer ce projet il vous faudra cloner le depot sur votre terminal avec la comande suivante:
```bash
git clone https://codefirst.iut.uca.fr/git/enzo.jolys/Kinect.git
```
Puis il suffiras d'ouvrir Visual Studio.
Vous pouvez alors maintenant lancer le projet en appuyant sur F5 ou en cliquant sur le bouton "Démarrer" dans Visual Studio tout en s'assurant que le projet de démarrage soit **Kinect**:
<br>
<img src="./images/startingProject.png" width="600"/>

# Règles du jeu

**Début du Jeu** : Le jeu démarre automatiquement cinq secondes après le lancement du projet.

**Objectif** : Le but du jeu est de protéger le village des licornes maléfiques. Ces licornes sont représentées par l'image fournie ci-dessous. Le joueur doit uniquement éliminer ces licornes maléfiques, évitant ainsi de toucher les licornes gentilles. Toucher une licorne gentilles ou manquer une licorne maléfique entraînera la défaite du joueur.

<br>
<img src="./images/badlicorn.png" width="600"/>

**Mécanique de Jeu** : Pour éliminer une licorne maléfique, le joueur doit viser avec sa main gauche et lever sa main droite au-dessus de sa tête pour exécuter l'action. La précision et la vitesse sont cruciales pour réussir.

**Victoire/Défaite** : Le joueur gagne s'il parvient à protéger le village en éliminant toutes les licornes maléfiques sans toucher une seule licorne gentilles. En revanche, toucher une licorne gentilles ou manquer une licorne maléfique conduit à la défaite.

# Structure de l'application

Voici un schéma des différentes composantes de notre projet:

```mermaid
flowchart LR
subgraph Exercise2
    MyGesturesBank
    KinectUtils
    Tests
end
MyGesturesBank -.-> KinectUtils 
KinectUtils -.-> Microsoft.Kinect
KinectUtils -.-> CommunityToolkit.Mvvm
Tests -.-> MyGesturesBank
Tests -.-> KinectUtils
style Microsoft.Kinect fill:#eee,color:#111,stroke:#999
style CommunityToolkit.Mvvm fill:#eee,color:#111,stroke:#999
subgraph Exercise3
    WpfApp
end
WpfApp -.-> MyGesturesBank
WpfApp -.-> KinectUtils
```

Voici un diagramme de classes du projet actuel pour le projet Model:

```mermaid
classDiagram
direction LR
class KinectStreamsFactory {
    +ctor(kinect: KinectManager)
    -streamFactory : Dictionary~KinectStreams, Func~KinectStream~~ 
    +this[stream: KinectStreams] : KinectStream
}

class KinectStreams {
    <<enum>>
    Color
    Depth
    IR
    CropBody
    Body
    BodyColor
}

KinectStreamsFactory --> "1" KinectManager
KinectStreamsFactory ..> KinectStreams
KinectStreamsFactory ..> KinectStream
KinectStream --> KinectManager
KinectStream <|-- ColorImageStream
KinectStream <|-- DepthImageStream
KinectStream <|-- InfraredImageStream
KinectStream <|-- BodyCropImageStream
KinectStream <|-- BodyImageStream
KinectStream <|-- BodyColorStream
```