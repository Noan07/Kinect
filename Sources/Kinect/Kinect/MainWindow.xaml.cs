﻿using KinectUtils;
using Microsoft.Kinect;
using MyGestureBank;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows.Navigation;
using VMWrapper;
using System.Windows.Threading;
using System.IO;
using System.Diagnostics;

namespace Kinect
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Random random = new Random();
        private Dictionary<string, int> touchedImages = new Dictionary<string, int>();
        private List<string>[] ponyImages = { new List<string>(), new List<string>() };
        public KinectViewModel ViewModel { get; set; }
        private int NbGood = 0;
        private int NbBad = 0;
        private const int NB_GOOD_WANTED = 10;
        private const int NB_BAD_WANTED = 3;
        private DispatcherTimer countdownTimer;
        private int countdownSeconds = 5;

        private CoordinateMapper CoordinateMapper { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            ViewModel = new KinectViewModel();
            Loaded += MainWindow_Loaded;
            ImageBackground.Source = new BitmapImage(new Uri(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "..\\..\\..\\images\\fond.jpg")));
            DataContext = this;
        }

        // InitializeKinect() de MainWindow
        private void InitializeKinect()
        {
            KinectSensor kinectSensor = KinectSensor.GetDefault();
            kinectSensor.Open();
            LeftHandEllipse.Visibility = Visibility.Visible;
            KinectManager kinectManager = new KinectManager();
            this.CoordinateMapper = kinectManager.kinectSensor.CoordinateMapper;
            GestureManager.StartAcquiringFrames(kinectManager);
            GestureManager.AddGestures(new AllGesturesFactory());
            GestureManager.GestureRecognized += Gesture_GestureRecognized;
            MapLeftHandToMouse leftHandMapping = new MapLeftHandToMouse(kinectSensor, this.CoordinateMapper);
            leftHandMapping.OnMapping += LeftHandMapping_OnMapping;

            GestureManager.AddMapping(leftHandMapping);

            DataContext = this;
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            StartCountdown();
        }
        private void LeftHandMapping_OnMapping(object sender, OnMappingEventArgs<System.Drawing.Point> e)
        {
            System.Drawing.Point handPosition = e.MappedValue;
            Debug.WriteLine(handPosition.Y);
            Debug.WriteLine(handPosition.X);
            Canvas.SetLeft(LeftHandEllipse, handPosition.X);
            Canvas.SetTop(LeftHandEllipse, handPosition.Y);
        }
        private void StartCountdown()
        {
            TextBlock.Visibility = Visibility.Visible;

            // Créer et configurer le DispatcherTimer
            countdownTimer = new DispatcherTimer();
            countdownTimer.Interval = TimeSpan.FromSeconds(1);
            countdownTimer.Tick += CountdownTimer_Tick;

            // Démarrer le timer
            countdownTimer.Start();
        }

        private void CountdownTimer_Tick(object sender, EventArgs e)
        {
            countdownSeconds--;

            // Mettre à jour le texte du TextBlock avec le compte à rebours
            TextBlock.Text = countdownSeconds.ToString();

            // Vérifier si le compte à rebours est terminé
            if (countdownSeconds <= 0)
            {
                countdownTimer.Stop();
                TextBlock.Visibility = Visibility.Collapsed;
                InitializeKinect();
                StartAnimations();
                CompositionTarget.Rendering += CompositionTarget_Rendering;
            }
        }

        private void StartAnimations()
        {
            ponyImages[0].Add("goodlicorn1");
            ponyImages[0].Add("goodlicorn2");
            ponyImages[0].Add("goodlicorn3");
            ponyImages[0].Add("goodlicorn4");
            ponyImages[1].Add("badlicorn");

            for (int i = 0; i < 3; i++)
            {
                NumberedImage image = new NumberedImage();
                string randomPonyImage;

                if (NbGood >= NB_GOOD_WANTED)
                    randomPonyImage = SelectRandomPony(1);
                else if (NbBad >= NB_BAD_WANTED)
                    randomPonyImage = SelectRandomPony(0);
                else
                    randomPonyImage = SelectRandomPony(random.Next(2));

                SetImageProperties(image, randomPonyImage);

                AnimateImage(image);
            }
        }

        private string SelectRandomPony(int type)
        {
            (type == 1 ? ref NbBad : ref NbGood)++;
            return ponyImages[type][random.Next(ponyImages[type].Count)];
        }


        private void SetImageProperties(NumberedImage image, string ponyImage)
        {
            image.Source = new BitmapImage(new Uri(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $"..\\..\\..\\images\\{ponyImage}.png")));
            image.Width = 300;
            image.Height = 300;
            image.Name = ponyImage;
        }

        private void AnimateImage(NumberedImage image)
        {
            double verticalPosition = MovingImage.ActualHeight > image.Height ? random.Next(0, (int)(MovingImage.ActualHeight - (image.Height - 50))) : 0;
            Canvas.SetLeft(image, MovingImage.ActualWidth + image.Width);
            Canvas.SetTop(image, verticalPosition);
            MovingImage.Children.Add(image);

            DoubleAnimation animation = new DoubleAnimation
            {
                From = MovingImage.ActualWidth,
                To = 0 - image.Width,
                Duration = TimeSpan.FromSeconds(random.Next(3, 8)),
                BeginTime = TimeSpan.FromSeconds(random.Next(0, 5)),
                RepeatBehavior = RepeatBehavior.Forever
            };

            image.BeginAnimation(Canvas.LeftProperty, animation);
        }

        private void CompositionTarget_Rendering(object sender, EventArgs e)
        {
            List<NumberedImage> imagesToRemove = new List<NumberedImage>(MovingImage.Children.Count);
            foreach (NumberedImage image in MovingImage.Children)
            {
                if (Canvas.GetLeft(image) + (image.ActualWidth - 10) < 0)
                    imagesToRemove.Add(image);
            }

            foreach (NumberedImage imageToRemove in imagesToRemove)
            {
                MovingImage.Children.Remove(imageToRemove);
                AddRandomPony();
            }
        }

        private void AddRandomPony()
        {
            if (NbGood + NbBad >= NB_GOOD_WANTED + NB_BAD_WANTED)
            {
                // Vérifier si le nombre total de poneys à l'écran a été atteint
                if (MovingImage.Children.Count == 0 && AreAllAnimationsComplete())
                {
                    TextBlock.Visibility = Visibility.Visible;
                    String TextToDisplay = touchedImages.ContainsKey("badlicorn") && touchedImages.Count == 1  ? "Vous avez gagné!" : "Vous avez perdu!";
                    TextBlock.Text = TextToDisplay;
                }
                return;
            }

            NumberedImage image = new NumberedImage();
            string randomPonyImage = NbGood >= NB_GOOD_WANTED ? SelectRandomPony(1) : NbBad >= NB_BAD_WANTED ? SelectRandomPony(0) : SelectRandomPony(random.Next(2));

            SetImageProperties(image, randomPonyImage);
            AnimateImage(image);

            if (!touchedImages.ContainsKey(randomPonyImage))
                touchedImages.Add(randomPonyImage, 1);
            else
                touchedImages[randomPonyImage]++;
        }

        private bool AreAllAnimationsComplete()
        {
            foreach (var image in MovingImage.Children)
            {
                if (image is NumberedImage numberedImage)
                {
                    return false;
                }
            }
            return true;
        }

        private void Gesture_GestureRecognized(object sender, GestureRecognizedEventArgs e)
        {
            ColorSpacePoint handPosition = CoordinateMapper.MapCameraPointToColorSpace(e.Body.Joints[JointType.HandLeft].Position);

            foreach (NumberedImage image in MovingImage.Children)
            {
                Point imagePosition = image.TranslatePoint(new Point(0, 0), MovingImage);

                if (handPosition.X >= imagePosition.X && handPosition.X <= imagePosition.X + image.Width &&
                    handPosition.Y >= imagePosition.Y && handPosition.Y <= imagePosition.Y + image.Height)
                {
                    string name = image.Name;
                    MovingImage.Children.Remove(image);
                    Animation_dead(Canvas.GetLeft(image), Canvas.GetTop(image));
                    AddRandomPony();
                    if (!touchedImages.ContainsKey(name))
                        touchedImages.Add(name, 1);
                    else
                        touchedImages[name]++;
                    break;
                }
            }
        }

        private void Animation_dead(double x,double y)
        {
            NumberedImage imageDead = new NumberedImage();
            imageDead.Source = new BitmapImage(new Uri(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $"..\\..\\..\\images\\blood.png")));
            imageDead.Width = 100;
            imageDead.Height = 100;
            Canvas.SetLeft(imageDead, x);
            Canvas.SetTop(imageDead, y);

            DeadEffect.Children.Add(imageDead);

            DoubleAnimation animation = new DoubleAnimation
            {
                From = y,
                To = MovingImage.ActualHeight + imageDead.Height,
                
            };
            imageDead.BeginAnimation(Canvas.TopProperty, animation);

            //DeadEffect.Children.Remove(imageDead);
        }
    }

    public class NumberedImage : Image { public int Number { get; set; } }
}

