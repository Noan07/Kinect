﻿using Microsoft.Kinect;
using System;

namespace KinectUtils
{
    public abstract class BaseGesture
    {
        public event EventHandler<GestureRecognizedEventArgs> GestureRecognized;
        public event EventHandler PostureRecognized;
        public event EventHandler PostureUnrecognized;
        private bool recognizedLastFrame = false;

        public abstract void TestGesture(Body body);

        protected virtual void OnGestureRecognized(object sender, GestureRecognizedEventArgs e)
        {
            GestureRecognized?.Invoke(this, e);
        }
    }
   
    public class GestureRecognizedEventArgs : EventArgs
    {
        public Body Body { get; }
        public Posture Posture { get; }

        public GestureRecognizedEventArgs(Body body, Posture posture = null)
        {
            Body = body;
            Posture = posture;
        }
    }
}