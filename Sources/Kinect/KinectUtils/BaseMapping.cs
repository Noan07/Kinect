﻿using Microsoft.Kinect;
using System;

namespace KinectUtils
{
    public abstract class BaseMapping<T>
    {
        protected bool running;

        public event EventHandler<OnMappingEventArgs<T>> OnMapping;
        private KinectSensor kinectSensor;
        private BodyFrameReader bodyFrameReader;


        // Constructeur
        protected BaseMapping(KinectSensor sensor)
        {
            kinectSensor = sensor;
            bodyFrameReader = kinectSensor.BodyFrameSource.OpenReader();
            bodyFrameReader.FrameArrived += OnBodyFrameArrived;
            running = true;
        }
        // Constructor that takes start and end gestures
        protected BaseMapping(BaseGesture startGesture, BaseGesture endGesture)
        {
            SubscribeToStartGesture(startGesture);
            SubscribeToEndGesture(endGesture);
            running = true; 
        }

        // Constructor that takes a toggle gesture
        protected BaseMapping(BaseGesture toggleGesture)
        {
            SubscribeToToggleGesture(toggleGesture);
            running = true;
        }

        public void SubscribeToStartGesture(BaseGesture startGesture)
        {
            startGesture.GestureRecognized += StartGestureRecognized;
        }

        public void SubscribeToEndGesture(BaseGesture endGesture)
        {
            endGesture.GestureRecognized += EndGestureRecognized;
        }

        public void SubscribeToToggleGesture(BaseGesture toggleGesture)
        {
            toggleGesture.GestureRecognized += ToggleGestureRecognized;
        }

        private void StartGestureRecognized(object sender, GestureRecognizedEventArgs e)
        {
            running = true;
        }

        private void EndGestureRecognized(object sender, GestureRecognizedEventArgs e)
        {
            running = false;
        }

        private void ToggleGestureRecognized(object sender, GestureRecognizedEventArgs e)
        {
            running = !running;
        }

        protected abstract T Mapping(Body body);

        protected abstract bool TestMapping(Body body, out T output);

        public virtual void OnBodyFrameArrived(object sender, BodyFrameArrivedEventArgs e)
        {
            using (var bodyFrame = e.FrameReference.AcquireFrame())
            {
                if (bodyFrame != null)
                {
                    Body[] bodies = new Body[bodyFrame.BodyCount];
                    bodyFrame.GetAndRefreshBodyData(bodies);

                    foreach (var body in bodies)
                    {
                        if (body.IsTracked && running)
                        {
                            if (TestMapping(body, out T output))
                            {
                                OnMapping?.Invoke(this, new OnMappingEventArgs<T>(output));
                            }
                        }
                    }
                }
            }
        }
    }

    public class OnMappingEventArgs<T> : EventArgs
    {
        public T MappedValue { get; }

        public OnMappingEventArgs(T mappedValue)
        {
            MappedValue = mappedValue;
        }
    }
}
