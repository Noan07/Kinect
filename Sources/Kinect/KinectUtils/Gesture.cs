﻿using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinectUtils
{
    public abstract class Gesture : BaseGesture
    {
        public bool IsTesting { get; private set; }
        protected int MinNbOfFrames { get; set; }
        protected int MaxNbOfFrames { get; set; }

        private int mCurrentFrameCount;

        protected abstract bool TestInitialConditions(Body body);
        protected abstract bool TestPosture(Body body);
        protected abstract bool TestRunningGesture(Body body);
        protected abstract bool TestEndingConditions(Body body);

        public override void TestGesture(Body body)
        {
            if (IsTesting)
            {
                if (!TestPosture(body) || !TestRunningGesture(body))
                {
                    IsTesting = false;
                }
                if (IsTesting && TestEndingConditions(body))
                {
                    if (mCurrentFrameCount >= MinNbOfFrames && mCurrentFrameCount <= MaxNbOfFrames)
                    {
                        OnGestureRecognized(this, new GestureRecognizedEventArgs(body));
                        IsTesting = false;
                    }
                }
                if (IsTesting) 
                {
                    mCurrentFrameCount++;
                }
            }
            else if (TestInitialConditions(body))
            {
                IsTesting = true;
                mCurrentFrameCount = 0;
            }
        }

    }

}
