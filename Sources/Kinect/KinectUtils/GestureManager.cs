﻿using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace KinectUtils
{
    public partial class GestureManager
    {
        private static readonly List<BaseGesture> KnownGestures = new List<BaseGesture>();
        private static bool isAcquiringFrames;
        private static KinectManager manager;
        private static BodyFrameReader bodyFrameReader;

        private static event EventHandler<GestureRecognizedEventArgs> gestureRecognizedEvent;
        public static event EventHandler<GestureRecognizedEventArgs> GestureRecognized
        {
            add
            {
                if (gestureRecognizedEvent == null)
                {
                    gestureRecognizedEvent += value;
                }
            }
            remove
            {
                gestureRecognizedEvent -= value;
            }
        }

        public static event EventHandler<BodyFrameArrivedEventArgs> BodyFrameArrived;


        public static void AddGestures(IGestureFactory factory)
        {
            var gestures = factory.CreateGestures();
            foreach (var gesture in gestures)
            {
                AddGesture(gesture);
            }
        }

        public static void AddGestures(params BaseGesture[] gestures)
        {
            foreach (var gesture in gestures)
            {
                AddGesture(gesture);
            }
        }

        private static void AddGesture(BaseGesture gesture)
        {
            if (!KnownGestures.Any(g => g.GetType() == gesture.GetType()))
            {
                gesture.GestureRecognized += Gesture_GestureRecognized;
                KnownGestures.Add(gesture);
            }
        }

        public static void RemoveGesture(BaseGesture gesture)
        {
            if (KnownGestures.Contains(gesture))
            {
                gesture.GestureRecognized -= Gesture_GestureRecognized;
                KnownGestures.Remove(gesture);
            }
        }

        public static void StartAcquiringFrames(KinectManager kinectManager)
        {
            if (!isAcquiringFrames)
            {
                manager = kinectManager;

                manager.kinectSensor.Open();
                bodyFrameReader = kinectManager.kinectSensor.BodyFrameSource.OpenReader();
                bodyFrameReader.FrameArrived += Reader_BodyFrameArrivedManager;
                isAcquiringFrames = true;
            }
        }

        public static void StopAcquiringFrame()
        {
            if (isAcquiringFrames)
            {
                manager.kinectSensor.Close();
                isAcquiringFrames = false;
                manager = null;
            }
        }

        private static void Reader_BodyFrameArrivedManager(object sender, BodyFrameArrivedEventArgs e)
        {
            using (var bodyFrame = e.FrameReference.AcquireFrame())
            {
                if (bodyFrame != null)
                {
                    Body[] bodies = new Body[bodyFrame.BodyCount];
                    bodyFrame.GetAndRefreshBodyData(bodies);
                    foreach (var body in bodies.Where(b => b.IsTracked))
                    {
                        if (body != null)
                        {
                            foreach (var gesture in KnownGestures)
                            {
                                gesture.TestGesture(body);
                            }
                        }
                    }
                }
            }
        }

        public static void AddMapping<T>(BaseMapping<T> mapping)
        {
            BodyFrameArrived += mapping.OnBodyFrameArrived;
        }

        public static void RemoveMapping<T>(BaseMapping<T> mapping)
        {
            BodyFrameArrived -= mapping.OnBodyFrameArrived;
        }

        private static void Reader_BodyFrameArrived(object sender, BodyFrameArrivedEventArgs e)
        {
            BodyFrameArrived?.Invoke(sender, e);
        }


        private static void Gesture_GestureRecognized(object sender, GestureRecognizedEventArgs e)
        {
            gestureRecognizedEvent?.Invoke(sender, e);
        }
    }
}

