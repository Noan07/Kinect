﻿using CommunityToolkit.Mvvm.ComponentModel;
using Microsoft.Kinect;

namespace KinectUtils
{
    /// <summary>
    /// Manages the Kinect sensor, start and stop the sensor.
    /// </summary>
    public partial class KinectManager : ObservableObject
    {
        /// <summary>
        /// KinectSensor instance.
        /// </summary>
        public KinectSensor kinectSensor { get; private set; }

        /// <summary>
        /// Kinect sensor.
        /// </summary>
        [ObservableProperty]
        [NotifyPropertyChangedFor(nameof(StatusText))]
        private bool status;

        /// <summary>
        /// Kinect sensor's status.
        /// </summary>
        public String StatusText => Status ? "Running" : "Not Running";

        /// <summary>
        /// Initializes a new instance of the KinectManager class.
        /// </summary>
        public KinectManager()
        {
            // Kinect sensor initialization

            // Retrieve the KinectSensor object
            this.kinectSensor = KinectSensor.GetDefault();

            // Set up the event notifier for IsAvailableChanged
            this.kinectSensor.IsAvailableChanged += this.Sensor_IsAvailableChanged;
        }

        /// <summary>
        /// Starts the Kinect sensor.
        /// </summary>
        public void StartSensor()
        {
            if (kinectSensor != null && !kinectSensor.IsOpen)
            {
                kinectSensor.Open();
            }
        }

        /// <summary>
        /// Stops the Kinect sensor.
        /// </summary>
        public void StopSensor()
        {
            if (kinectSensor != null && kinectSensor.IsOpen)
            {
                kinectSensor.Close();
            }
        }

        /// <summary>
        /// Handles the event when the sensor's availability changes.
        /// </summary>
        private void Sensor_IsAvailableChanged(object sender, IsAvailableChangedEventArgs e)
        {
            // Update the status based on the sensor's availability
            this.Status = this.kinectSensor.IsAvailable;
        }
    }

}