﻿using Microsoft.Kinect;
using System.Drawing;

namespace KinectUtils
{
    public class MapLeftHandToMouse : BaseMapping<Point>
    {
        private readonly CoordinateMapper coordinateMapper;

        public MapLeftHandToMouse(KinectSensor sensor, CoordinateMapper coordinateMapper) : base(sensor)
        {
            this.coordinateMapper = coordinateMapper;
        }

        protected override Point Mapping(Body body)
        {
            ColorSpacePoint colorHandPosition = coordinateMapper.MapCameraPointToColorSpace(body.Joints[JointType.HandLeft].Position);
            Point mappedValue = new Point((int)colorHandPosition.X, (int)colorHandPosition.Y);
            return mappedValue;
        }

        protected override bool TestMapping(Body body, out Point output)
        {
            output = Mapping(body);
            return true;
        }
    }
}
