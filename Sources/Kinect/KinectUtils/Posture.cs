﻿using Microsoft.Kinect;
using System;

namespace KinectUtils
{
    public abstract class Posture : BaseGesture
    {
        private bool recognizedLastFrame = false;
        private Dictionary<ulong, bool> recognizedAtLastEvents = new();
        public override void TestGesture(Body body)
        {
            recognizedAtLastEvents.TryGetValue(body.TrackingId, out bool recognizedAtLastEvent);

            if (!recognizedAtLastEvents.ContainsKey(body.TrackingId))
            {
                recognizedAtLastEvents[body.TrackingId] = false;
            }

            bool recognizedReal = TestPosture(body);

            if (recognizedReal && !recognizedAtLastEvent)
            {
                OnGestureRecognized(this, new(body, this));
                OnPostureRecognized(this, new(body, this));
            }
            else if (!recognizedReal && recognizedAtLastEvent)
            {
                OnPostureUnrecognized(this, new(body, this));
            }

            recognizedAtLastEvents[body.TrackingId] = recognizedReal;
        }


        protected abstract bool TestPosture(Body body);

        private void OnPostureRecognized(object sender, GestureRecognizedEventArgs e)
        {
            PostureRecognized?.Invoke(this, e);
        }

        private void OnPostureUnrecognized(object sender, GestureRecognizedEventArgs e)
        {
            PostureUnrecognized?.Invoke(this, e);
        }

        public event EventHandler PostureRecognized;
        public event EventHandler PostureUnrecognized;
    }
}
