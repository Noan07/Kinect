﻿using CommunityToolkit.Mvvm.ComponentModel;
using KinectUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Model
{
    public partial class BasicImageStream : KinectStream
    {
        public BasicImageStream(KinectManager manager) : base(manager)
        {
        }

        /// <summary>
        /// ObservableProperty WriteableBitmap used for displaying the stream image.
        /// </summary>
        [ObservableProperty]
        private WriteableBitmap streamToDisplay;

        public override void Start()
        {
            throw new NotImplementedException();
        }

        public override void Stop()
        {
            throw new NotImplementedException();
        }
    }
}
