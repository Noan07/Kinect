﻿using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using KinectUtils;
using Microsoft.Kinect;

namespace Model
{
    public class BodyImageStream : KinectStream
    {
        private Body[] bodies;
        private BodyFrameReader bodyFrameReader;
        private CoordinateMapper CoordinateMapper { get; set; }

        public ObservableCollection<Shape> StreamToDisplayCanvas { get; set; } = new ObservableCollection<Shape>();


        public BodyImageStream(KinectManager kinectManager) : base(kinectManager)
        {
            Start();
            this.bodies = new Body[kinectManager.kinectSensor.BodyFrameSource.BodyCount];
            this.CoordinateMapper = this.kinectManager.kinectSensor.CoordinateMapper;
            this.bodyFrameReader = kinectManager.kinectSensor.BodyFrameSource.OpenReader();
            this.bodyFrameReader.FrameArrived += Reader_BodyFrameArrived;

            
        }

        private void Reader_BodyFrameArrived(object sender, BodyFrameArrivedEventArgs e)
        {
            
            using (var bodyFrame = e.FrameReference.AcquireFrame())
            {
                if (bodyFrame != null)
                {
                    bodyFrame.GetAndRefreshBodyData(this.bodies);

                    // Reset the collection
                    StreamToDisplayCanvas.Clear();

                    foreach (Body body in this.bodies)
                    {
                        if (body.IsTracked)
                        {
                            DrawBody(body);
                        }
                    }
                }
            }
        }

        private void DrawBody(Body body)
        {

            foreach (JointType jointType in body.Joints.Keys)
            {
                Joint joint = body.Joints[jointType];

                if (joint.TrackingState == TrackingState.Tracked)
                {

                    ColorSpacePoint colorPoint = CoordinateMapper.MapCameraPointToColorSpace(joint.Position);


                    Ellipse jointEllipse = new Ellipse
                    {
                        Width = 10,
                        Height = 10,
                        Fill = Brushes.Red
                    };

                    Canvas.SetLeft(jointEllipse, (colorPoint.X)  - jointEllipse.Width / 2);
                    Canvas.SetTop(jointEllipse, (colorPoint.Y)  - jointEllipse.Height / 2);
                    StreamToDisplayCanvas.Add(jointEllipse);

                }
            }

            DrawAllBone(body);
        }

        private void DrawAllBone(Body body)
        {
            DrawBone(body, JointType.Head, JointType.Neck);
            DrawBone(body, JointType.Neck, JointType.SpineShoulder);

            // Wrist - Elbow
            DrawBone(body, JointType.WristLeft, JointType.ElbowLeft);
            DrawBone(body, JointType.WristRight, JointType.ElbowRight);

            // Shoulder - Elbow
            DrawBone(body, JointType.ElbowLeft, JointType.ShoulderLeft);
            DrawBone(body, JointType.ElbowRight, JointType.ShoulderRight);

            // Shoulder - Neck
            DrawBone(body, JointType.ShoulderLeft, JointType.Neck);
            DrawBone(body, JointType.ShoulderRight, JointType.Neck);

            // Knee - Foot
            DrawBone(body, JointType.KneeLeft, JointType.FootLeft);
            DrawBone(body, JointType.KneeRight, JointType.FootRight);

            // Knee - Hip
            DrawBone(body, JointType.KneeLeft, JointType.HipLeft);
            DrawBone(body, JointType.KneeRight, JointType.HipRight);

            // Hip - SpinBase
            DrawBone(body, JointType.HipLeft, JointType.SpineBase);
            DrawBone(body, JointType.HipRight, JointType.SpineBase);

            // Spine
            DrawBone(body, JointType.SpineBase, JointType.SpineMid);
            DrawBone(body, JointType.SpineMid, JointType.SpineShoulder);

        }

        private void DrawBone(Body body, JointType jointType1, JointType jointType2)
        {
            CameraSpacePoint position1 = body.Joints[jointType1].Position;
            ColorSpacePoint colorPoint1 = CoordinateMapper.MapCameraPointToColorSpace(position1);

            CameraSpacePoint position2 = body.Joints[jointType2].Position;
            ColorSpacePoint colorPoint2 = CoordinateMapper.MapCameraPointToColorSpace(position2);


            // Condition moins l'infini
            // System.ArgumentException : ''-∞' n'est pas une valeur valide pour la propriété 'X2'.'
            if (colorPoint1.X < 0 || colorPoint1.Y < 0 || colorPoint2.X < 0 || colorPoint2.Y < 0)
            {
                //Debug.WriteLine("Val negatif");
                return;
            }

            Line boneLine = new Line
            {
                X1 = colorPoint1.X ,
                Y1 = colorPoint1.Y,
                X2 = colorPoint2.X,
                Y2 = colorPoint2.Y,
                Stroke = Brushes.Blue,
                StrokeThickness = 4
            };
            StreamToDisplayCanvas.Add(boneLine);
        }

        public override void Start()
        {
        }

        public override void Stop()
        {
            if (bodyFrameReader != null)
            {
                bodyFrameReader.Dispose();
                bodyFrameReader = null;
            }
        }

    }
}