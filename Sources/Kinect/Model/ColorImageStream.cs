﻿using Microsoft.Kinect;
using System.Diagnostics;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using KinectUtils;

namespace Model
{
    /// <summary>
    /// Represents a color image stream from the Kinect sensor.
    /// </summary>
    public class ColorImageStream : BasicImageStream
    {
        private ColorFrameReader colorFrameReader = null;

        /// <summary>
        /// Initializes a new instance of the ColorImageStream class.
        /// </summary>
        public ColorImageStream(KinectManager kinectManager) : base(kinectManager)
        {
            Start();

            // Open the reader for the color frames
            this.colorFrameReader = this.kinectManager.kinectSensor.ColorFrameSource.OpenReader();

            // Wire the frame arrival event handler
            this.colorFrameReader.FrameArrived += this.Reader_ColorFrameArrived;

            // Create the colorFrameDescription from the ColorFrameSource using RGBA format
            FrameDescription colorFrameDescription = this.kinectManager.kinectSensor.ColorFrameSource.CreateFrameDescription(ColorImageFormat.Rgba);

            // Initialize the WriteableBitmap
            this.StreamToDisplay = new WriteableBitmap(colorFrameDescription.Width, colorFrameDescription.Height, 96.0, 96.0, PixelFormats.Bgr32, null);
        }

        public override void Start()
        {
            // Open the sensor
            this.kinectManager.StartSensor();
        }

        public override void Stop()
        {
            // Close the reader and stop the sensor
            if (colorFrameReader != null)
            {
                colorFrameReader.Dispose();
                colorFrameReader = null;
            }
        }

        /// <summary>
        /// Handles the color frame data arriving from the sensor.
        /// </summary>
        private void Reader_ColorFrameArrived(object sender, ColorFrameArrivedEventArgs e)
        {
            // Acquire the color frame
            using (ColorFrame colorFrame = e.FrameReference.AcquireFrame())
            {
                if (colorFrame != null)
                {
                    // Retrieve color frame description
                    FrameDescription colorFrameDescription = colorFrame.FrameDescription;

                    // Lock the streamToDisplay for writing
                    this.StreamToDisplay.Lock();

                    try
                    {
                        // Check if the image size has changed
                        if ((colorFrameDescription.Width == this.StreamToDisplay.PixelWidth) && (colorFrameDescription.Height == this.StreamToDisplay.PixelHeight))
                        {
                            // Copy color frame data to the WriteableBitmap
                            colorFrame.CopyConvertedFrameDataToIntPtr(
                                this.StreamToDisplay.BackBuffer,
                                (uint)(colorFrameDescription.Width * colorFrameDescription.Height * 4),
                                ColorImageFormat.Bgra);

                            // Update the dirty rect for rendering
                            this.StreamToDisplay.AddDirtyRect(new Int32Rect(0, 0, this.StreamToDisplay.PixelWidth, this.StreamToDisplay.PixelHeight));
                        }
                    }
                    finally
                    {
                        // Unlock the streamToDisplay
                        this.StreamToDisplay.Unlock();
                    }
                }
                else
                {
                    Debug.WriteLine("Null color frame.");
                }
            }
        }
    }
}
