﻿using Microsoft.Kinect;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using KinectUtils;

namespace Model
{
    public class DepthImageStream : BasicImageStream
    {
        private DepthFrameReader depthFrameReader = null;
        private const int MapDepthToByte = 8000 / 256;
        private byte[] bitmapPixels = null;

        public DepthImageStream(KinectManager kinectManager) : base(kinectManager)
        {
            Start();

            // Open the reader for depth frames
            this.depthFrameReader = this.kinectManager.kinectSensor.DepthFrameSource.OpenReader();

            // Attach handler for frame arrival
            this.depthFrameReader.FrameArrived += this.Reader_DepthFrameArrived;

            // Create the depth frame description from the depth frame source
            FrameDescription depthFrameDescription = this.kinectManager.kinectSensor.DepthFrameSource.FrameDescription;

            this.StreamToDisplay = new WriteableBitmap(depthFrameDescription.Width, depthFrameDescription.Height, 96.0, 96.0, PixelFormats.Gray8, null);
            this.bitmapPixels = new byte[depthFrameDescription.Width * depthFrameDescription.Height];
        }

        /// Handles the depth frame data arriving from the sensor.
        private void Reader_DepthFrameArrived(object sender, DepthFrameArrivedEventArgs e)
        {
            using (DepthFrame depthFrame = e.FrameReference.AcquireFrame())
            {
                if (depthFrame != null)
                {
                    FrameDescription depthFrameDescription = depthFrame.FrameDescription;

                    using (KinectBuffer depthBuffer = depthFrame.LockImageBuffer())
                    {
                        this.StreamToDisplay.Lock();

                        // Check if the image size has changed
                        if ((depthFrameDescription.Width == this.StreamToDisplay.PixelWidth) && (depthFrameDescription.Height == this.StreamToDisplay.PixelHeight))
                        {
                            // Process the depth frame
                            ProcessDepthFrameData(depthBuffer.UnderlyingBuffer, depthBuffer.Size, depthFrame.DepthMinReliableDistance, depthFrame.DepthMaxReliableDistance);

                            // Apply pixels to the streamToDisplay
                            RenderDepthPixels();
                        }

                        this.StreamToDisplay.Unlock();
                    }
                }
            }
        }

        /// Handles the depth frame data and converts it to grayscale.
        private unsafe void ProcessDepthFrameData(IntPtr depthFrameData, uint depthFrameDataSize, ushort minDepth, ushort maxDepth)
        {
            // Convert the depth frame to a visual representation in grayscale
            ushort* frameData = (ushort*)depthFrameData;

            for (int i = 0; i < (int)(depthFrameDataSize / sizeof(ushort)); ++i)
            {
                ushort depth = frameData[i];

                // Map depth to grayscale
                byte intensity = (byte)(depth >= minDepth && depth <= maxDepth ? (depth / MapDepthToByte) : 0);

                // Apply pixels to the image
                this.bitmapPixels[i] = intensity;
            }
        }

        /// Renders depth pixels into the writeableBitmap.
        private void RenderDepthPixels()
        {
            this.StreamToDisplay.WritePixels(
                new Int32Rect(0, 0, this.StreamToDisplay.PixelWidth, this.StreamToDisplay.PixelHeight),
                this.bitmapPixels,
                this.StreamToDisplay.PixelWidth,
                0);
        }

        /// Handles the depth stream start.
        public override void Start()
        {
            this.kinectManager.StartSensor();
        }

        /// Handles the depth stream stop.
        public override void Stop()
        {
            if (depthFrameReader != null)
            {
                depthFrameReader.Dispose();
                depthFrameReader = null;
            }
        }
    }

}
