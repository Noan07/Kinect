﻿using Microsoft.Kinect;
using KinectUtils;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Model
{
    public class InfraredImageStream : BasicImageStream
    {
        private InfraredFrameReader infraredFrameReader = null;

        // Ajoutez ces constantes
        private const float InfraredSourceValueMaximum = (float)ushort.MaxValue;
        private const float InfraredSourceScale = 0.75f;
        private const float InfraredOutputValueMinimum = 0.01f;
        private const float InfraredOutputValueMaximum = 1.0f;

        public InfraredImageStream(KinectManager kinectManager) : base(kinectManager)
        {
            Start();

            // open the reader for the infrared frames
            this.infraredFrameReader = this.kinectManager.kinectSensor.InfraredFrameSource.OpenReader();

            // wire handler for frame arrival
            this.infraredFrameReader.FrameArrived += this.Reader_InfraredFrameArrived;

            // create the infraredFrameDescription from the InfraredFrameSource
            FrameDescription infraredFrameDescription = this.kinectManager.kinectSensor.InfraredFrameSource.FrameDescription;

            this.StreamToDisplay = new WriteableBitmap(infraredFrameDescription.Width, infraredFrameDescription.Height, 96.0, 96.0, PixelFormats.Gray32Float, null);
        }

        /// Handles the infrared frame data arriving from the sensor.
        private void Reader_InfraredFrameArrived(object sender, InfraredFrameArrivedEventArgs e)
        {
            using (InfraredFrame infraredFrame = e.FrameReference.AcquireFrame())
            {
                if (infraredFrame != null)
                {
                    FrameDescription infraredFrameDescription = infraredFrame.FrameDescription;

                    using (KinectBuffer infraredBuffer = infraredFrame.LockImageBuffer())
                    {
                        this.StreamToDisplay.Lock();

                        // Check if the size of the image has changed
                        if ((infraredFrameDescription.Width == this.StreamToDisplay.PixelWidth) && (infraredFrameDescription.Height == this.StreamToDisplay.PixelHeight))
                        {
                            ProcessInfraredFrameData(infraredBuffer.UnderlyingBuffer, infraredBuffer.Size);
                            this.StreamToDisplay.AddDirtyRect(new Int32Rect(0, 0, this.StreamToDisplay.PixelWidth, this.StreamToDisplay.PixelHeight));
                        }

                        this.StreamToDisplay.Unlock();
                    }
                }
            }
        }

        // Process the infrared frame data to create a displayable streamToDisplay.
        private unsafe void ProcessInfraredFrameData(IntPtr infraredFrameData, uint infraredFrameDataSize)
        {
            ushort* frameData = (ushort*)infraredFrameData;
            float* backBuffer = (float*)this.StreamToDisplay.BackBuffer;

            // Process the infrared data
            for (int i = 0; i < (int)(infraredFrameDataSize / this.kinectManager.kinectSensor.InfraredFrameSource.FrameDescription.BytesPerPixel); ++i)
            {
                backBuffer[i] = Math.Min(InfraredOutputValueMaximum, (((float)frameData[i] / InfraredSourceValueMaximum * InfraredSourceScale) * (1.0f - InfraredOutputValueMinimum)) + InfraredOutputValueMinimum);
            }
        }

        public override void Start()
        {
            this.kinectManager.StartSensor();
        }

        public override void Stop()
        {
            if (infraredFrameReader != null)
            {
                infraredFrameReader.Dispose();
                infraredFrameReader = null;
            }
        }
    }
}
