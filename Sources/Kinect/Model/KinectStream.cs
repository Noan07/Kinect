﻿using CommunityToolkit.Mvvm.ComponentModel;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using KinectUtils;

namespace Model
{
    /// <summary>
    /// KinectStream class : Base class for Kinect streams
    /// </summary>
    public abstract partial class KinectStream : ObservableObject
    {
        /// <summary>
        /// Gets the KinectManager.
        /// </summary>
        public KinectManager kinectManager { get; protected set; }

        /// <summary>
        /// Initializes a new instance of the KinectStream class.
        /// </summary>
        public KinectStream(KinectManager manager)
        {
            kinectManager = manager;
        }

        /// <summary>
        /// Starts the Kinect stream.
        /// </summary>
        public abstract void Start();

        /// <summary>
        /// Stops the Kinect stream.
        /// </summary>
        public abstract void Stop();
    }

}
