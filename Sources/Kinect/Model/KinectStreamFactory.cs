﻿using KinectUtils;

namespace Model
{
    /// <summary>
    /// Factory class for creating instances of Kinect streams
    /// </summary>
    public class KinectStreamFactory
    {
        private Dictionary<KinectStreams, Func<KinectStream>> streamFactory;

        /// <summary>
        /// Initializes a new instance of the KinectStreamFactory"/> class.
        /// </summary>
        /// <param name="kinectManager">The KinectManager instance used for stream creation.</param>
        public KinectStreamFactory(KinectManager kinectManager)
        {
            streamFactory = new Dictionary<KinectStreams, Func<KinectStream>>
            {
                { KinectStreams.Color, () => new ColorImageStream(kinectManager) },
                { KinectStreams.Depth, () => new DepthImageStream(kinectManager) },
                { KinectStreams.IR, () => new InfraredImageStream(kinectManager) },
                { KinectStreams.Body, () => new BodyImageStream(kinectManager) },
                { KinectStreams.BodyColor, () => new BodyColorStream(kinectManager) },
                { KinectStreams.CoordinateMapping, () => new CoordinateMappingImageStream(kinectManager) },
            };
        }

        /// <summary>
        /// Gets the KinectStream instance based on the specified stream type.
        /// </summary>
        public KinectStream this[KinectStreams streamType] => streamFactory[streamType]();
    }

}
