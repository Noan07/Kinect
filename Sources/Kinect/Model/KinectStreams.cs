﻿namespace Model
{
    /// <summary>
    /// Enumeration representing various Kinect streams.
    /// </summary>
    public enum KinectStreams
    {
        None,
        Color,
        Depth,
        IR,
        BodyColor,
        Body,
        CoordinateMapping,
    }
}
