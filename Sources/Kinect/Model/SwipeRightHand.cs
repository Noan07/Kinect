﻿using Microsoft.Kinect;
using System;
using System.Diagnostics;

namespace KinectUtils
{
    public class SwipeRightHand : Gesture
    {
        private CameraSpacePoint initialRightHandPosition;

        public SwipeRightHand()
        {
            // Set your desired values for MinNbOfFrames and MaxNbOfFrames
            MinNbOfFrames = 10;
            MaxNbOfFrames = 30;
        }

        protected override bool TestInitialConditions(Body body)
        {
            // Check if the right hand is initially open and within the specified position range
            if (IsHandBetweenHeadAndHip(body.Joints[JointType.HandRight].Position, body.Joints[JointType.Head].Position, body.Joints[JointType.SpineBase].Position))
            {
                initialRightHandPosition = body.Joints[JointType.HandRight].Position;
                return true;
            }

            return false;
        }

        private bool IsHandBetweenHeadAndHip(CameraSpacePoint handPosition, CameraSpacePoint headPosition, CameraSpacePoint hipPosition)
        {
            // Adjust the Y range based on your specific requirements
            float minY = headPosition.Y;
            float maxY = hipPosition.Y;

            return  handPosition.Y < maxY;
        }


        protected override bool TestPosture(Body body)
        {
            // Check if the right hand is within the specified position range for posture
            return IsHandBetweenHeadAndHip(body.Joints[JointType.HandRight].Position, body.Joints[JointType.Head].Position, body.Joints[JointType.SpineBase].Position);
        }

        protected override bool TestRunningGesture(Body body)
        {
            CameraSpacePoint currentRightHandPosition = body.Joints[JointType.HandRight].Position;

            float thresholdX = 0.10f;
            float thresholdZ = 0.30f;
            //currentRightHandPosition.X > initialRightHandPosition.X + thresholdX
             //   &&
            // Check if the hand is moving right within the specified thresholds
            bool isMovingRight = Math.Abs(currentRightHandPosition.Z - initialRightHandPosition.Z) < thresholdZ;

            Console.WriteLine(isMovingRight);

            return isMovingRight;
        }

        protected override bool TestEndingConditions(Body body)
        {
            CameraSpacePoint currentRightHandPosition = body.Joints[JointType.HandRight].Position;

            // You can adjust the threshold based on your specific requirements
            float thresholdX = 0.10f;
            float thresholdZ = 0.10f;

            // Check if the hand has returned to the initial position within the specified thresholds
            bool hasReturnedToInitialPosition = currentRightHandPosition.X < initialRightHandPosition.X - thresholdX
                && Math.Abs(currentRightHandPosition.Z - initialRightHandPosition.Z) < thresholdZ;

            return hasReturnedToInitialPosition;
        }
    }
}
