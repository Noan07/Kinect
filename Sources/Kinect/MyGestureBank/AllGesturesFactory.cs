﻿
using KinectUtils;

namespace MyGestureBank
{
    public class AllGesturesFactory : IGestureFactory
    {
        public IEnumerable<BaseGesture> CreateGestures()
        {
            return new List<BaseGesture>
            {
                new PostureRightHandUp(),
            };
        }
    }

}
