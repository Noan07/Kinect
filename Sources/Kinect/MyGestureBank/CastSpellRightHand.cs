﻿using KinectUtils;
using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGestureBank
{
    public class CastSpellRightHand : Gesture
    {
        private CameraSpacePoint lastRightElbowPosition;
        private CameraSpacePoint lastRightHandPosition;

        public CastSpellRightHand()
        {
            MinNbOfFrames = 3;
            MaxNbOfFrames = 30;
        }

        protected override bool TestEndingConditions(Body body)
        {
            CameraSpacePoint currentElbowRightPosition = body.Joints[JointType.ElbowRight].Position;
            CameraSpacePoint currentHandRightPosition = body.Joints[JointType.HandRight].Position;

            bool areElbowAndHandAligned = Math.Abs(lastRightElbowPosition.X - currentHandRightPosition.X) < 0.1;

            return true;
        }


        protected override bool TestInitialConditions(Body body)
        {
            CameraSpacePoint currentElbowRightPosition = body.Joints[JointType.ElbowRight].Position;
            CameraSpacePoint currentHandRightPosition = body.Joints[JointType.HandRight].Position;
            lastRightHandPosition = currentHandRightPosition;
            lastRightElbowPosition = currentElbowRightPosition;
            return currentElbowRightPosition.X - currentHandRightPosition.X < 0.07 && currentElbowRightPosition.Y < currentHandRightPosition.Y;
        }

        protected override bool TestPosture(Body body)
        {
            CameraSpacePoint currentElbowRightPosition = body.Joints[JointType.ElbowRight].Position;
            return lastRightElbowPosition.Y - currentElbowRightPosition.Y < 0.02 && lastRightHandPosition.X - currentElbowRightPosition.X < 0.02;
        }

        protected override bool TestRunningGesture(Body body)
        {
            CameraSpacePoint currentElbowRightPosition = body.Joints[JointType.ElbowRight].Position;
            CameraSpacePoint currentHandRightPosition = body.Joints[JointType.HandRight].Position;

            var Condition = currentHandRightPosition.Y < lastRightHandPosition.Y;
            lastRightHandPosition = currentHandRightPosition;
            return Condition;
        }
    }
}
