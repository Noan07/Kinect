﻿using KinectUtils;
using Microsoft.Kinect;

namespace MyGestureBank
{
    public class ClapHands : Gesture
    {

        private CameraSpacePoint lastLeftHandPosition;
        private CameraSpacePoint lastRightHandPosition;

        public ClapHands()
        {
            MinNbOfFrames = 10;
            MaxNbOfFrames = 30;
        }

        protected override bool TestEndingConditions(Body body)
        {
            float threshold = 0.05f;

            bool areHandsClose = Math.Abs(body.Joints[JointType.HandLeft].Position.X - body.Joints[JointType.HandRight].Position.X) < threshold;
            return areHandsClose;
        }

        protected override bool TestInitialConditions(Body body)
        {
            CameraSpacePoint currentLeftHandPosition = body.Joints[JointType.HandLeft].Position;
            CameraSpacePoint currentRightHandPosition = body.Joints[JointType.HandRight].Position;
            CameraSpacePoint currentShoulderPosition = body.Joints[JointType.SpineShoulder].Position;
            CameraSpacePoint currentMidPosition = body.Joints[JointType.SpineBase].Position;



            bool isWithinDistanceX = Math.Abs(currentLeftHandPosition.X) - Math.Abs(currentRightHandPosition.X) < Math.Abs(currentShoulderPosition.Y  - currentMidPosition.Y);
            bool isWithinRangeY = IsHandBetweenHeadAndSpinBase(body.Joints[JointType.HandRight].Position, body.Joints[JointType.Head].Position, body.Joints[JointType.SpineBase].Position);
            return isWithinDistanceX && isWithinRangeY;
        }

        protected override bool TestPosture(Body body)
        {

            bool isWithinRangeY = IsHandBetweenHeadAndSpinBase(body.Joints[JointType.HandRight].Position, body.Joints[JointType.Head].Position, body.Joints[JointType.SpineBase].Position);
            return isWithinRangeY;
        }

        protected override bool TestRunningGesture(Body body)
        {
            CameraSpacePoint currentLeftHandPosition = body.Joints[JointType.HandLeft].Position;
            CameraSpacePoint currentRightHandPosition = body.Joints[JointType.HandRight].Position;

            bool areHandsCloserThanLastFrame =
                Math.Abs(currentLeftHandPosition.X) <= lastLeftHandPosition.X + 0.1f &&
                Math.Abs(currentRightHandPosition.X) <= lastRightHandPosition.X + 0.1f;

            lastLeftHandPosition = currentLeftHandPosition;
            lastRightHandPosition = currentRightHandPosition;

            return areHandsCloserThanLastFrame;
        }
        private bool IsHandBetweenHeadAndSpinBase(CameraSpacePoint handPosition, CameraSpacePoint headPosition, CameraSpacePoint hipPosition)
        {
            float maxY = headPosition.Y;
            float minY = hipPosition.Y;


            return handPosition.Y < maxY && handPosition.Y > minY;
        }

    }
}
