﻿using KinectUtils;
using Microsoft.Kinect;

namespace MyGestureBank
{
    public class PostureRightHandUp : Posture
    {
        protected override bool TestPosture(Body body)
        {
            return body.Joints[JointType.HandLeft].Position.Y < body.Joints[JointType.Head].Position.Y
                && body.Joints[JointType.HandRight].Position.Y > body.Joints[JointType.Head].Position.Y;
        }
    }
}
