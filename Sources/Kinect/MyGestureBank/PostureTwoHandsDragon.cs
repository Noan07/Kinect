﻿using KinectUtils;
using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGestureBank
{
    public class PostureTwoHandsDragon : Posture
    {
        private const double MarginWristClose = 0.002;
        private const double YMarginElbowAligned = 0.05;

        protected override bool TestPosture(Body body)
        {
            CameraSpacePoint leftHand = body.Joints[JointType.HandLeft].Position;
            CameraSpacePoint rightHand = body.Joints[JointType.HandRight].Position;
            double leftElbowY = body.Joints[JointType.ElbowLeft].Position.Y;
            double rightElbowY = body.Joints[JointType.ElbowRight].Position.Y;

            bool wristsClose = Math.Abs(leftHand.Y - rightHand.Y) < MarginWristClose && Math.Abs(leftHand.X - rightHand.X) < MarginWristClose;
            bool elbowsAligned = Math.Abs(leftElbowY - rightElbowY) < YMarginElbowAligned;

            return wristsClose && elbowsAligned;
        }
    }
}
