﻿using KinectUtils;
using Microsoft.Kinect;
using System;
using System.Diagnostics;

namespace MyGestureBank
{
    public class SwipeRightHand : Gesture
    {
        private CameraSpacePoint initialRightHandPosition;

        public SwipeRightHand()
        {
            MinNbOfFrames = 10;
            MaxNbOfFrames = 30;
        }

        protected override bool TestInitialConditions(Body body)
        {
            return IsHandBetweenHeadAndSpinBody(body.Joints[JointType.HandRight].Position, body.Joints[JointType.Head].Position, body.Joints[JointType.SpineBase].Position);
        }

        private bool IsHandBetweenHeadAndSpinBody(CameraSpacePoint handPosition, CameraSpacePoint headPosition, CameraSpacePoint hipPosition)
        {
            float maxY = headPosition.Y;
            float minY = hipPosition.Y;

            return handPosition.Y < maxY && handPosition.Y > minY;
        }

        protected override bool TestPosture(Body body)
        {
            return IsHandBetweenHeadAndSpinBody(body.Joints[JointType.HandRight].Position, body.Joints[JointType.Head].Position, body.Joints[JointType.SpineBase].Position);
        }

        protected override bool TestRunningGesture(Body body)
        {
            CameraSpacePoint currentRightHandPosition = body.Joints[JointType.HandRight].Position;

            float thresholdX = 0.10f;
            float thresholdZ = 0.03f;
            if(initialRightHandPosition.Z == 0)
            {
                initialRightHandPosition = currentRightHandPosition;
            }
            bool isMovingRight = currentRightHandPosition.Z <= initialRightHandPosition.Z && currentRightHandPosition.X <= initialRightHandPosition.X;

            initialRightHandPosition = currentRightHandPosition;
            return isMovingRight;
        }

        protected override bool TestEndingConditions(Body body)
        {
            CameraSpacePoint currentRightShoulderPosition = body.Joints[JointType.ShoulderRight].Position;

            float thresholdZ = 0.10f;

            bool hasReturnedToInitialPosition = Math.Abs(currentRightShoulderPosition.X - initialRightHandPosition.X) < thresholdZ;
            return hasReturnedToInitialPosition;
        }
    }
}
