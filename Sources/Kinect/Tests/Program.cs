﻿using KinectUtils;
using Microsoft.Kinect;
using MyGestureBank;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests
{
    class Program
    {
        static void Main(string[] args)
        {
            testGestureSlap();
        }

        static void testGestureSlap()
        {
            KinectSensor kinectSensor = KinectSensor.GetDefault();
            kinectSensor.Open();

            KinectManager kinectManager = new KinectManager();

            // Initialize Kinect manager and start acquiring frames
            GestureManager.StartAcquiringFrames(kinectManager);

            IGestureFactory gestureFactory = new AllGesturesFactory();

            // Add gestures to GestureManager
            GestureManager.AddGestures(gestureFactory);

            // Subscribe to GestureRecognized event
            GestureManager.GestureRecognized += KinectManager2_GestureRecognizedEvent;
            Console.WriteLine("Press Enter to exit...");
            Console.ReadLine();
            // Stop acquiring frames and close Kinect sensor
            GestureManager.StopAcquiringFrame();

            Console.WriteLine("Press Enter to exit...");
            Console.ReadLine();
        }

        static void testPostureTwoHandsDragon()
        {
            // Exemple d'utilisation avec une instance de Kinect
            KinectSensor kinectSensor = KinectSensor.GetDefault();
            kinectSensor.Open();

            KinectManager kinectManager = new KinectManager();

            // Instancier une posture
            Posture postureTwoHandsDragon = new PostureTwoHandsDragon();

            // Écouter les événements de reconnaissance de posture
            postureTwoHandsDragon.PostureRecognized += PostureRecognizedHandler;
            postureTwoHandsDragon.PostureUnrecognized += PostureUnrecognizedHandler;

            // Exécuter le test de posture à chaque frame
            kinectManager.kinectSensor.BodyFrameSource.OpenReader().FrameArrived += (sender, eventArgs) =>
            {
                using (var frame = eventArgs.FrameReference.AcquireFrame())
                {
                    if (frame != null)
                    {
                        // Obtenez les données des corps
                        Body[] bodies = new Body[frame.BodyFrameSource.BodyCount];
                        frame.GetAndRefreshBodyData(bodies);

                        // Obtenez le premier corps suivi
                        Body body = bodies.FirstOrDefault(b => b.IsTracked);
                        if (body != null)
                        {
                            // Testez la posture
                            postureTwoHandsDragon.TestGesture(body);
                        }

                    }
                }
            };

            Console.ReadLine();
        }
        static void testPostureRightHandUp()
        {
            // Exemple d'utilisation avec une instance de Kinect
            KinectSensor kinectSensor = KinectSensor.GetDefault();
            kinectSensor.Open();

            KinectManager kinectManager = new KinectManager();

            // Instancier une posture
            Posture postureRightHandUp = new PostureRightHandUp();

            // Écouter les événements de reconnaissance de posture
            postureRightHandUp.GestureRecognized += PostureRecognizedHandler;
            postureRightHandUp.PostureUnrecognized += PostureUnrecognizedHandler;

            // Exécuter le test de posture à chaque frame
            kinectManager.kinectSensor.BodyFrameSource.OpenReader().FrameArrived += (sender, eventArgs) =>
            {
                using (var frame = eventArgs.FrameReference.AcquireFrame())
                {
                    if (frame != null)
                    {
                        // Obtenez les données des corps
                        Body[] bodies = new Body[frame.BodyFrameSource.BodyCount];
                        frame.GetAndRefreshBodyData(bodies);

                        // Obtenez le premier corps suivi
                        Body body = bodies.FirstOrDefault(b => b.IsTracked);
                        if (body != null)
                        {
                            // Testez la posture
                            postureRightHandUp.TestGesture(body);
                        }

                    }
                }
            };

            Console.ReadLine();
        }


        static void PostureRecognizedHandler(object sender, EventArgs e)
        {
            Console.WriteLine("Posture Recognized!");
        }

        static void PostureUnrecognizedHandler(object sender, EventArgs e)
        {
            Console.WriteLine("Posture Unrecognized!");
        }

        static void KinectManager_GestureRecognizedEvent(object sender, GestureRecognizedEventArgs e)
        {
            Console.WriteLine($"Gesture recognized: ");
        }
        static void KinectManager2_GestureRecognizedEvent(object sender, GestureRecognizedEventArgs e)
        {
            Console.WriteLine($"Gesture recognized: 2");
        }

    }
}
