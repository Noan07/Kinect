﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using Model;
using System.ComponentModel;
using KinectUtils;

namespace VMWrapper
{

    /// <summary>
    /// ViewModel for Kinect functionality
    /// </summary>
    public partial class KinectViewModel : ObservableObject
    {
        // Observable properties
        [ObservableProperty]
        private KinectManager kinectManager;
        [ObservableProperty]
        private KinectStream stream ;
        public KinectStreamFactory StreamFactory { get; set; }

        /// <summary>
        /// Constructor for KinectViewModel
        /// </summary>
        public KinectViewModel() 
        {
            KinectManager = new KinectManager();
            KinectManager.PropertyChanged += (object sender, PropertyChangedEventArgs e) => this.ChangeStreamToDisplayCommand.NotifyCanExecuteChanged();
            
        }


        private bool CanChangeStream => KinectManager.Status;

        /// <summary>
        /// Command to change the Kinect stream to display
        /// </summary>
        [RelayCommand(CanExecute = nameof(CanChangeStream))]
        private void ChangeStreamToDisplay(KinectStreams KinectStream)
        {
            Stream.Stop();
            Stream = StreamFactory[KinectStream];
        }

        /// <summary>
        /// Command to start the Kinect sensor
        /// </summary>
        [RelayCommand]
        private void StartSensor()
        {
            KinectManager.StartSensor();
            StreamFactory = new KinectStreamFactory(KinectManager);

            Stream = StreamFactory[KinectStreams.Color];
        }

        /// <summary>
        /// Command to start the Kinect sensor for Game
        /// </summary>
        [RelayCommand]
        private void StartSensorGame()
        {
            KinectManager.StartSensor();
            StreamFactory = new KinectStreamFactory(KinectManager);

            Stream = StreamFactory[KinectStreams.CoordinateMapping];
        }

        /// <summary>
        /// Command to stop the Kinect sensor
        /// </summary>
        [RelayCommand]
        private void StopSensor()
        {
            Stream.Stop();
            KinectManager.StopSensor();
        }
    }
}