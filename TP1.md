[Home](./README.md)

**Critères de notation TP1**
@ | category | description | status 
--- | --- | --- | --- 
☢️ | | the repositories must be accessible by the teacher | ☢️ 
☢️ | | a .gitignore file must be use at the creation of the repository or at the first push | ☢️
🎬 | | all *class libraries* and *applications* build | ✅ 
🎬 | | *applications* run without any bug | ✅
🚨🟢 | XAML of the Window | | ✅
🚨🟢 | Kinect Connection | Use of the Kinect sensor | ✅
🚨🟢 | Kinect Connection | Use of the Kinect event | ✅
🚨🟢🔖 | Kinect Connection | Encapsulation | ✅
🚨🟢 | Kinect Connection | Connect when the Window is loaded | ✅
🚨🟢 | Kinect Connection | Disconnect when the Window is unloaded | ✅
🚨🟢 | Kinect Connection | Status feedback in the Window | ✅
🔴🔖 | Kinect Connection | Using MVVM (commands for window loaded and unloaded) | ✅
🟡🔖 | Kinect Connection | Using MVVM (status feedback) | ✅
🚨🟢🔖 | Color Stream | Encapsulation - ColorImageStream class with **ONLY** necessary members (beware of mindless copy and paste) | ✅
🚨🟢 | Color Stream | retrieving the frame and processing it | ✅
🚨🟢 | Color Stream | start and stop acquisition | ✅
🚨🟢 | Color Stream | updating the window (start when clicking the button) | ✅
🚨🟢 | Color Stream | updating the window (display the color stream) | ✅
🟡🔖 | Color Stream | Using MVVM (button click + commands) | ✅
🟢🔖 | Color Stream | Using MVVM (data bind the color stream to the image) | ✅
🎬🚨🟢 | Color Stream | *test* runs without any bug | ✅
🟢🔖 | Depth Stream | Encapsulation - DepthImageStream class with **ONLY** necessary members (beware of mindless copy and paste) | ✅
🟢 | Depth Stream | retrieving the frame and processing it | ✅
🟢 | Depth Stream | start and stop acquisition | ✅
🟢 | Depth Stream | updating the window (start when clicking the button, stop when clicking another button) | ✅
🟢 | Depth Stream | updating the window (display the color stream) | ✅
🟡🔖 | Depth Stream | Using MVVM (button click + commands) | ✅
🟢🔖 | Depth Stream | Using MVVM (data bind the depth stream to the image) | ✅
🎬🟢 | Depth Stream | *test* runs without any bug | ✅
🟢🔖 | IR Stream | Encapsulation - InfraredImageStream class with **ONLY** necessary members (beware of mindless copy and paste) | ✅
🟢 | IR Stream | retrieving the frame and processing it | ✅
🟢 | IR Stream | start and stop acquisition | ✅
🟢 | IR Stream | updating the window (start when clicking the button, stop when clicking another button) | ✅
🟢 | IR Stream | updating the window (display the IR stream) | ✅
🟡🔖 | IR Stream | Using MVVM (button click + commands) | ✅
🟢🔖 | IR Stream | Using MVVM (data bind the infrared stream to the image) | ✅
🎬🟢 | IR Stream | *test* runs without any bug | ✅
🟡🔖 | Architecture | Abstract class to factorize | ✅
🔴🔖 | Architecture | Abstract class to factorize (using MVVM) | ✅
🔴🔖 | Architecture | Factory to allow choosing between one stream or another | ✅
🎬🔴🔖 | Architecture | *test* runs without any bug using the architecture and MVVM | ✅
🚨🟢🔖 | Body Stream | Encapsulation - BodyStream class with **ONLY** necessary members (beware of mindless copy and paste) | ✅
🚨🟢 | Body Stream | retrieving the frame and processing it, retrieving the body of the closest user | ➖ we have chosen to display all body
🚨🟢 | Body Stream | start and stop acquisition | ✅
🚨🟡 | Body Stream | fill a Canvas with Ellipses and Lines corresponding to the skeleton | ✅
🚨🟢 | Body Stream | updating the window (start when clicking the button, stop when clicking another button) | ✅
🚨🟡 | Body Stream | updating the window (display the body stream) | ✅
🔴🔖 | Body Stream | Using MVVM (button click + commands) | ✅
🔴🔖 | Body Stream | Using MVVM (data bind the body stream to the image) | ✅
🎬🚨🔴 | Body Stream | *test* runs without any bug | ✅
🔴🔖 | Architecture | Use the Body Stream with the abstract class and the Factory to allow choosing between one stream or another | ✅
🎬🔴🔖 | Architecture | *test* runs without any bug using the architecture and MVVM | ✅
🟡🔖 | Body and Color Streams | Encapsulation - BodyAndColorStream class (be smart) | ✅
🟡🔖 | Body and Color Streams | updating the window (start when clicking the button, stop when clicking another button) | ✅
🔴🔖 | Body and Color Streams | updating the window (display the body stream) | ✅
🔴🔖 | Body and Color Streams | Using MVVM (button click + commands) | ✅
🔴🔖 | Body and Color Streams | Using MVVM (data bind the body and color streams to the image) | ✅
🎬🔴🔖 | Body and Color Streams | *test* runs without any bug | ✅
🎬🔴🔖 | Architecture | *test* runs without any bug using the architecture and MVVM | ✅
🟢🔖 | Architecture | Create a class library | ✅
🎬🟢 | Documentation | ReadMe, comments, wiki... | ✅ we didn't use the wiki
