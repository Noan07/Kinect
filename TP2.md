

**Grading criteria TP2**

**Part 1**

@ | category | description | coeff 
--- | --- | --- | --- 
☢️ | | the repositories must be accessible by the teacher | ☢️ 
☢️ | | a .gitignore file must be use at the creation of the repository or at the first push | ☢️
🎬 | | all *class libraries* and *applications* build | ✅
🎬 | | *applications* run without any bug | ✅
🚨🟢🔖 | KinectUtils class library | with KinectManager | ✅
🚨🟢🔖 | KinectUtils class library | BaseGesture with event | ✅
🚨🟢🔖 | KinectUtils class library | Posture | ✅
🚨🟢 | KinectUtils class library | Posture.TestPosture | ✅
🚨🟡🔖 | MyGesturesBank class library | 1st posture | ✅
🟡 | MyGesturesBank class library | 2nd posture | ✅
🚨🟡 | Test | Console test | ✅
🚨🟢🔖 | GestureManager | collection of gestures | ✅
🚨🟡🔖 | GestureManager | acquiring frames | ✅
🚨🟡 | GestureManager | event (dirty but working) | ✅
🔴🔖 | GestureManager | event automatic subscriptions to collection of gestures | ❌
🔴🔖 | GestureManager | event late subscriptions | ❌
🚨🟡 | Test | Console test | ✅
🟡🔖 | Posture specific events | PostureRecognized and PostureUnrecognized | ✅
🟡🔖 | Posture specific events | Update GestureManager | 6
🟡 | Test | Console test | ✅
🎬🟢 | Documentation | ReadMe, comments, wiki... | ✅

**Part 2**

@ | category | description | coeff 
--- | --- | --- | --- 
☢️ | | the repositories must be accessible by the teacher | ☢️ 
☢️ | | a .gitignore file must be use at the creation of the repository or at the first push | ☢️
🎬 | | all *class libraries* and *applications* build | ✅
🎬 | | *applications* run without any bug | ✅
🚨🟡 | Gesture | TestGesture | ✅
🚨🟡 | Gesture | 1st subclass | ✅
🔴 | Gesture | use body referential instead of kinect referential | ✅
🚨🟢 | Test | Console Test | ✅
🚨🟢🔖 | IGestureFactory | interface and implementation | ✅
🚨🟢 | Test | Console Test | ✅
🟡 | Gesture | 2nd subclass | ✅
🟢 | Test | Console Test | ✅
🎬🟢 | Documentation | ReadMe, comments, wiki... | ✅

**Part 3**

@ | category | description | coeff 
--- | --- | --- | --- 
☢️ | | the repositories must be accessible by the teacher | ☢️ 
☢️ | | a .gitignore file must be use at the creation of the repository or at the first push | ☢️
🎬 | | all *class libraries* and *applications* build | ✅
🎬 | | *applications* run without any bug | ✅
🚨🟡 | Mapping | TestGesture | ✅
🚨🟡 | GestureManager | BodyFrameArrived | ✅
🟢 | IGestureFactory | indexer | ❌
🔴 | ConcreteMapping | BodyFrameArrived | ❌
🚨🟢 | Test | Console Test | ❌
🔴🔖 | OnMapping | BodyFrameArrived + OnMapping | ✅
🟢 | Test | Console Test | ❌
🎬🟢 | Documentation | ReadMe, comments, wiki... | ✅