
**Grading criteria TP3**

@ | description | coeff 
--- | --- | ---
☢️ | the repositories must be accessible by the teacher | ☢️ 
☢️ | a .gitignore file must be use at the creation of the repository or at the first push | ☢️
🎬 | all *class libraries* and *applications* build | ✅
🎬 | *applications* run without any bug | ✅
🚨🟢 | Use of gestures recognition (postures and/or gestures and/or mapping) | ✅
🚨🟡 | Use of content (shapes, images, animations...) | ✅
🚨🔴 | Interactive application | ✅
🎬🟢 | Documentation (how to play, how to run the app...) | ✅